﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace MVVM
{
    public class ViewModelMain : ObservableObject
    {

        private bool hatDach;
        public bool HatDach
        {
            get { return hatDach; }
            set
            {
                hatDach = value;
                RaisePropertyChangedEvent("HatDach");
                RaisePropertyChangedEvent("IstFertig");
            }
        }

        private bool hatFenster;
        public bool HatFenster
        {
            get { return hatFenster; }
            set
            {
                hatFenster = value;
                RaisePropertyChangedEvent("HatFenster");
                RaisePropertyChangedEvent("IstFertig");
            }
        }

        private bool hatTueren;
        public bool HatTueren
        {
            get { return hatTueren; }
            set
            {
                hatTueren = value;
                RaisePropertyChangedEvent("HatTueren");
                RaisePropertyChangedEvent("IstFertig");
            }
        }
        public bool IstFertig
        {
            get { return HatDach && HatTueren && HatFenster; }
            set
            {
                if(value == false)
                {
                    return;
                }
                HatDach = HatFenster = HatTueren = true;
                RaisePropertyChangedEvent("IstFertig");
            }
        }


        private string input;

        public string Input
        {
            get { return input; }
            set
            {
                input = value;
                Output = value;
            }
        }


        private string output;

        public string Output
        {
            get { return output; }
            set
            {
                output = value.ToUpper().Replace("ß", "ss");
                RaisePropertyChangedEvent("Output");
            }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                if (value.Length > 10)
                    Foreground = Brushes.Black;
                else
                    Foreground = Brushes.LightCoral;
                //RaisePropertyChangedEvent("Email");
            }
        }

        private Brush foreground = Brushes.LightCoral;

        public Brush Foreground
        {
            get { return foreground; }
            set
            {
                foreground = value;
                RaisePropertyChangedEvent("Foreground");
            }
        }

        public ICommand CommandButtonClick
        {
            get { return new DelegateCommand(ButtonSaveClick); }
        }

        private void ButtonSaveClick()
        {
            Output = "Button wurde....";
        }
    }

    class DelegateCommand : ICommand
    {
        private readonly Action action;
        public DelegateCommand(Action action)
        {
            this.action = action;
        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            action();
        }
    }

}
